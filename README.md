# Install #
Clone repo: 

	git clone https://Celina_C@bitbucket.org/Celina_C/kolubryna.git
	cd kolubryna

Install virtualenv: 

	sudo apt-get install python-pip python-dev build-essential

	sudo pip install virtualenv

	sudo pip install --upgrade pip

Create new virtualenv:

	virtualenv kolubryna
	cd kolubryna 
	source bin/activate

Install packages:

	pip install -r requirements.txt

Run project:

	python manage.py runserver

Open browser:

	http://127.0.0.1:8000