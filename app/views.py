from django.shortcuts import render_to_response
from django.template import RequestContext
import math


def index(request):
    text = 45
    if request.method == 'POST':
        text = request.POST.get('kat', '')

    return render_to_response('index.html', {'kat': text}, context_instance=RequestContext(request))


def game():
    return 0